import { configureStore, ThunkAction, Action } from '@reduxjs/toolkit';
import homeReducer from '../features/home/homeSlice';
import loaderReducer from '../components/Loader/loaderSlice';

export const store = configureStore({
  reducer: {
    home: homeReducer,
    loader: loaderReducer,
  },
});

export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;
