import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import Home from './features/home';
import Movies from './features/movies';
import Series from './features/series';
import './App.css';
import {store} from "./app/store";

function App() {
  return (
      <BrowserRouter>
        <Switch>
            <Route path={'/movies'}  store={store} component={Movies} />
            <Route path={'/series'} store={store} component={Series} />
            <Route store={store} component={Home} />
        </Switch>
      </BrowserRouter>
  );
}

export default App;
