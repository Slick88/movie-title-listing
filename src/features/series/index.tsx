import React, {useCallback, useState} from 'react';
import PageWrapper from "../../containers/PageWrapper";
import InteractionCard from "../../components/InteractionCard";
import Table from "../../components/Table";
import {useSelector} from "react-redux";
import {selectSeries, selectSpecial} from "../home/homeSlice";
import {Button, Modal} from "react-bootstrap";

export const Home = () => {
    const seriesData = useSelector(selectSeries)
    const movieSpecialData = useSelector(selectSpecial)
    const [showModal, toggleModal] = useState(false)
    const [modalData, setModalData] = useState({
        title: undefined,
        description: undefined,
        releaseYear: undefined
    })

    const handleClose = () => toggleModal(false);
    const handleShow = (values: any) => {
        console.log(values);
        setModalData(values);
        toggleModal(true)
    };

    const getFilteredData = useCallback(() => {
        return seriesData.map(values => <InteractionCard onclick={() => handleShow(values)} image={values.images['Poster Art'].url} title={values.title}/>);
    }, [seriesData]);

    return (
        <>
            <Table tableList={getFilteredData()} />
            <Modal show={showModal} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>{modalData.title}</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div>Description: {modalData.description}</div>
                    <br/>
                    <div>Release Year: {modalData.releaseYear}</div>
                    <br/>
                    <div>Special Movie Data: {movieSpecialData}</div>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                        Close
                    </Button>
                    <Button variant="primary" onClick={handleClose}>
                        Save Changes
                    </Button>
                </Modal.Footer>
            </Modal>
        </>
    );
}

export default PageWrapper(Home);