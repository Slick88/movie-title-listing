import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { AppThunk, RootState } from '../../app/store';
import JSONData from '../../sample.json'
import axios from 'axios';
import { enableHB, disableHB } from '../../components/Loader/loaderSlice';

interface HomeState {
  total: number;
  entries: Array<any>;
  special: string
}

const initialState: HomeState = {
  total: 0,
  entries: [],
  special: ''
};

export const homeSlice = createSlice({
  name: 'home',
  initialState,
  reducers: {
    setTitles: (state, action: PayloadAction<any>) => {
      state.entries = action.payload.entries;
      state.total = action.payload.total;
      state.special = action.payload.special;
    }
  },
});

export const { setTitles } = homeSlice.actions;

export const loadData = (): AppThunk => (dispatch) => {
  // turn on loader screen
    dispatch(enableHB())

    // Call magical api here
  const url = 'http://numbersapi.com'

  return axios.get(`${url}/42`, )
      .then((response) => {
        console.log(response);
          setTimeout(() => {
              // @ts-ignore
              JSONData.special = response.data
              dispatch(setTitles(JSONData));
          }, 2500)
        dispatch(disableHB())
      })
      .catch((e) => {
        console.log(e);
        //  dispatch toast message and status
      });

  // turn off loader
};

export const selectMovies = (state: RootState) => state.home.entries.filter(element => element.programType === 'movie');
export const selectSeries = (state: RootState) => state.home.entries.filter(element => element.programType === 'series');
export const selectSpecial = (state: RootState) => state.home.special

export default homeSlice.reducer;
