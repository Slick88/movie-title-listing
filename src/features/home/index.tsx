import React, {useEffect} from 'react';
import PageWrapper from "../../containers/PageWrapper";
import InteractionCard from '../../components/InteractionCard';
import { useHistory } from "react-router-dom";
import Table from '../../components/Table';
import {useDispatch} from "react-redux";
import {loadData} from "./homeSlice";

interface Props {
    location: {
        pathname: string;
    }
}

export const Home = (props: Props) => {
    let history = useHistory();
    let dispatch = useDispatch()



    useEffect(() => {
        // Update the document title using the browser API
        dispatch(loadData())
    },  []);

    let data = [
        <InteractionCard onclick={() => history.push("/series")} image={'https://streamcoimg-a.akamaihd.net/000/110/1196/1101196-PosterArt-c47d98ddca64ef18cf79c78d6ab8b3ad.jpg'} title={'Popular Series'}/>,
        <InteractionCard onclick={() => history.push("/movies")} image={'https://streamcoimg-a.akamaihd.net/000/110/1196/1101196-PosterArt-c47d98ddca64ef18cf79c78d6ab8b3ad.jpg'} title={'Popular Movies'}/>
    ]

    return (
        <>
            <Table tableList={data} />
            </>
    );
}

export default PageWrapper(Home);