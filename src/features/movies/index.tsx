import React, {useCallback, useState} from 'react';
import PageWrapper from "../../containers/PageWrapper";
import InteractionCard from "../../components/InteractionCard";
import Table from "../../components/Table";
import {useSelector} from "react-redux";
import {selectMovies, selectSpecial} from "../home/homeSlice";
import { Modal, Button } from 'react-bootstrap';



export const Movie = () => {
    const movieData = useSelector(selectMovies)
    const movieSpecialData = useSelector(selectSpecial)

    const [showModal, toggleModal] = useState(false)
    const [modalData, setModalData] = useState({
        title: undefined,
        description: undefined,
        releaseYear: undefined
    })

    const handleClose = () => toggleModal(false);
    const handleShow = (values: any) => {
        console.log(values);
        setModalData(values);
        toggleModal(true)
    };

    const getFilteredData = useCallback(() => {
        return movieData.map(values => <InteractionCard onclick={() => handleShow(values)} image={values.images['Poster Art'].url} title={values.title}/>);
    }, [movieData]);

    return (
        <>
            <Table tableList={getFilteredData()}/>
            <Modal show={showModal} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>{modalData.title}</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div><b>Description:</b> {modalData.description}</div>
                    <br/>
                    <div><b>Release Year:</b> {modalData.releaseYear}</div>
                    <br/>
                    <div><b>Special Movie Data:</b> {movieSpecialData}</div>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                        Close
                    </Button>
                    <Button variant="primary" onClick={handleClose}>
                        Save Changes
                    </Button>
                </Modal.Footer>
            </Modal>
        </>
    );
}

export default PageWrapper(Movie);