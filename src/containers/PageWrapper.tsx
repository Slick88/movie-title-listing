import React  from 'react';
import { Container } from 'react-bootstrap'
import Navbar from '../components/NavBar';
import Footer from "../components/Footer";
import {getLoader} from "../components/Loader/loaderSlice";
import {useSelector} from "react-redux";
import Loader from "../components/Loader";

export interface Props {
    location: {
        pathname: string;
    };
}

const PageWrapper = (WrapperComponents: any) => {
    return (props: Props) => {
        // eslint-disable-next-line react-hooks/rules-of-hooks
        const {status} = useSelector(getLoader)

        return (
            <>
                {status && <Loader/>}
                <Navbar location={props.location.pathname}/>
                <Container fluid>
                    <WrapperComponents {...props} />
                </Container>
             </>
        );
    };
};

export default PageWrapper;
