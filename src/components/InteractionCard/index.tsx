import React, {Component} from 'react';
import { Card } from 'react-bootstrap';

export interface Props {
    title: string,
    image: string,
    onclick(): void,
}

class InteractionCard extends Component<Props> {
    render() {
        return (
            <Card onClick={this.props.onclick} style={{ width: '8rem', border: 'none' }}>
                <Card.Img variant="top" src={this.props.image} />
                <Card.Text>{this.props.title}</Card.Text>
            </Card>
        );
    }
}

export default InteractionCard;