import React, {Component} from 'react';
import { Navbar } from "react-bootstrap";

export interface Props {
    location: string;
}

class NavBar extends Component <Props> {
    getPageTitle = () => {
        switch (this.props.location) {
            case '/series':
                return 'Series'
            case '/movies':
                return 'Movies'
            default:
                return 'Titles'
        }
    }

    render() {
        return (
            <>
                <Navbar bg="light" expand="lg">
                    <Navbar.Brand href="/">Demo Streaming</Navbar.Brand>
                    <div className={'justify-content-end'} style={{width: '100%'}}>
                        <button>additional buttons</button>
                    </div>
                </Navbar>
                <Navbar bg="light" expand="lg">
                    <Navbar.Brand>Popular {this.getPageTitle()}</Navbar.Brand>
                </Navbar>
            </>
        );
    }
}

export default NavBar;