import React, {Component} from 'react';
import {Spinner} from "react-bootstrap";
import './Loader.css'

class Loader extends Component {
    render() {
        return (
            <div className={'loader'}>
                <Spinner animation="border" />
            </div>
        );
    }
}

export default Loader;