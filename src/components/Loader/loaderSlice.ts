import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import {RootState} from '../../app/store';

interface LoaderState {
    status: boolean;
    message: string;
}

const initialState = {
    status: false,
    message: ""
};

export const loaderSlice = createSlice({
    name: 'loader',
    initialState,
    reducers: {
        enableHB: (state, action: PayloadAction<string|undefined>) => {
            state.status = true;
            if (action.payload) {
                state.message = action.payload
            }
        },
        disableHB: (state) => {
            state.status = false;
            state.message = '';
        },
    },
});

export const getLoader = (state: RootState) => state.loader;

export const { enableHB, disableHB } = loaderSlice.actions;

export default loaderSlice.reducer;
