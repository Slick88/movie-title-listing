import React, {Component} from 'react';
import InteractionCard from '../InteractionCard';
import { Table as RBTable } from 'react-bootstrap';


interface Props {
    tableList: any[]
}

class Table extends Component<Props> {

    //todo need to add in filler <td>
    renderRow = (rowData: any[]) => rowData.map((item) => <td>{item}</td>);

    renderCol() {
        const { tableList } = this.props;
        const numberOfTableContent = tableList.length;
        /**
         * break the main array into row sized chunks for row rendering
         * @type {Array}
         */
        const newContentChunks = [];
        for (let i = 0; i < numberOfTableContent; i += 6) {
            newContentChunks.push(tableList.slice(i, i + 6));
        }
        return newContentChunks.map((rowD) => <tr style={{width: '8rem'}}>{this.renderRow(rowD)}</tr>);
    }
    render() {
        return (
            <RBTable size={'sm'}>
                <tbody>{this.renderCol()}</tbody>
            </RBTable>
        );
    }
}

export default Table;