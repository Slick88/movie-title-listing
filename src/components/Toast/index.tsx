import React, {Component} from 'react';
import {Toast as RBToast} from 'react-bootstrap'

interface Props {

}

interface State {
    show: boolean
}

class Toast extends Component <Props, State> {
    constructor(props: Props) {
        super(props);
        this.state = {
            show: false
        }
    }

    setShow = (stateValue: boolean) => {
        this.setState({
            show: stateValue
        })
    }

    render() {
        const {show} = this.state;

        return (
            <RBToast onClose={() => this.setShow(false)} show={show} delay={3000} autohide>
                <RBToast.Header>
                    <img
                        src="holder.js/20x20?text=%20"
                        className="rounded mr-2"
                        alt=""
                    />
                    <strong className="mr-auto">Bootstrap</strong>
                    <small>11 mins ago</small>
                </RBToast.Header>
                <RBToast.Body>Woohoo, you're reading this text in a Toast!</RBToast.Body>
            </RBToast>
        );
    }
}

export default Toast;